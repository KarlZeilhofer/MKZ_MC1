



/*
Projekt Massekomparator mit einer Semi-Mikrowaage von Sartorius: MC210S

Aufbau:
	Waage mit Gehänge für 2 Massen A+B
		evt. Kalibriergewicht K
		evt. Pendelbremseinrichtung

Kommunikations-Kette:
	Server mit USB
	Arduino Mega 2560, (Serial)
	Arduino, Serial2
	RS232-TTL-Umsetzer
	Waage

	Verwendet wird Software Handshake (Xon, Xoff)

Auf dem Arduino hängen noch:
	BME280 (Umgebungssensor mit Druck, Feuchte und Temperatur)
	RTC-Chip DS3231
	MC210-ServoController:
		Modellbauservos (Lindinger MC-1000MGBB):

			zwei für Masse A
			zwei für Masse B

			Evt:
				(Einer für das bremsen der Pendelbewegung des Gehänges) <-- nicht notwendig
				Einer für Kalibriergewicht K
				einer für die zyklische Anregung der Waage, um ein "LSB-Rauschen" zu erzeugen,
					das dann der Mittelung dient. (Die Auflösung kann durch Mitteln nicht weiter erhöht werden,
					wenn der Messwert konstant ist. In der PTB wird über 30 Minuten oder so ähnlich gemittelt -
					hier treten definitiv LSB-Schwankungen auf. )

	Mess Zyklus: A B B A
	oder evt. A B B+K A

Funktionen der Software:
	 * Servoansteuerung, wurde ausgelagert auf eigenes Board: MC210-ServoController mit Arduino Nano
 	 * Implementierung des Messzyklus
 	 * evt. mit Konstantbelastungseinrichtung: Regelung der Servobewegungen beim Gewichtswechsel
 	 * Datenweiterleitung: leitet alles in beide Richtungen einfach durch,
 	 	 wobei jedoch zusätzliche Befehle vom Server empfangen werden können,
 	 	 um entsprechend die Servos anzusteuern.
 	 * Messwerte mit einem Zeitstempel versehen.
	 * auswerten zusätzlicher Sensoren: Umgebungstemperatur, Luftdruck, Luftfeuchte
   * Temperaturregelung mit Frostschutz auf 27°C, Überhitzungsschutz mit 34°C und Nenntemperatur von 32°C

Remote Flashing:
	Der Arduino hängt auf dem Server via USB (/dev/ttyACM0)
	Unter dem Pfad MC1/2-firmware/binaries
	befindet sich die mcflash.mk Datei, ein Makefile.
	Diese kann mit "make -f flash.mk" gestartet werden.
	die entsprechende hex-Datei, MC210_Massekomparator.ino.hex, muss zuvor per SFTP oder git dorthin kopiert werden.

*/

#include <Arduino.h>
#include <OneWire.h>
#include <Adafruit_BME280.h>
#include "EEPROMAnything.h"
#include <Wire.h>
#include "Sodaq_DS3231.h"
#include "Time.h" // this must be included after Sodaq_DS3231.h (!!!)

// Konstanten
	#define N_RX 100
	#define XOFF (byte)19
	#define XON (byte)17

	#define PIN_HEATING 52

// Makros:
	#define debug(x) //Serial.print(x)

// Globale Variablen
	Adafruit_BME280 bme280; // Environment Sensor for Temperature, Pressure and Humidity
	double T = 25; // Temperatur in 0.01°C
	double Tavg = T; // Mittlere Temperatur, über den letzten Messzyklus (siehe nAvg)
	double p = 101300; // Luftdruck in Pa
	double phi = 40; // relative Luftfeuchte in %
	double P = 0; // Heizleistung in Watt

	bool heatingAutoMode = true;
	bool saveRegState = false;
	bool loadRegState = false;
	bool calibrating = false;
	bool interpreterMode = false;
	
	
	bool loadMassA = false;
	bool loadMassB = false;
	bool cyclicABBA = false;
	enum State{State_01, State_A1, State_02, State_B1, State_03, State_B2, State_04, State_A2, State_05,
		State_End}; // Endstate is just for modulo
	int massState = State_01;


// Funktionen
	void testCmd();
	void printDateTime();
	bool serial2WaitForLine(char* templ, int32_t timeOut_ms);
	char* serial2ReadLine(int32_t timeOut_ms);
	float getTemperature();
	float getPressure();
	float getHumidity();
	void testBme280();
	void switchHeating(bool onFlag);
	void temperatureController();
	void weighingController();
	void syncTime();
	void testRTC();
	void MC210_setFilter();
	void MC210_startCalibration();



//The setup function is called once at startup of the sketch
void setup()
{
// Add your initialization code here

	Serial.begin(9600); // zum/vom PC
	Serial2.begin(1200, SERIAL_7O1); // zur/von Waage

	MC210_setFilter();

	Serial.println("MC210_Massekomparator");
	Serial.print("built on ");
	Serial.print(__DATE__);
	Serial.print(", ");
	Serial.println(__TIME__);

	pinMode(PIN_HEATING, OUTPUT);
	digitalWrite(PIN_HEATING, LOW); // Heizung ausschalten!

	rtc.begin(); // DS3231
	//testRTC();

	bme280.begin(0x76); // I2C connection
	//testBme280();
}

// The loop function is called in an endless loop
void loop()
{
	static double lastWeighingValue = -1000;

	// Akkumulatoren für die Mittelwertbildung
	static double sum_p=0;
	static double sum_phi=0;
	static double sum_T=0;
	static int32_t nAvg=0;

	// Empfangenes Zeichen:
	uint8_t ch;

	static bool lastChWasEsc = false;
	static bool arduinoCommand = false;


	// Sensoren einlesen:
	static int32_t oldMillis=-1001;
	if((int32_t)millis() >= oldMillis + 1000){
		Serial2.write(XOFF);
		oldMillis += 1000;
		T = getTemperature();
		p = getPressure();
		phi = getHumidity();

		sum_T += T;
		sum_p += p;
		sum_phi += phi;
		nAvg++;
		Serial2.write(XON);

		Tavg = sum_T/nAvg;
	}


	// Daten vom PC:
	if(Serial.available()){
		// Weiterleiten an die Waage
		Serial.readBytes(&ch, 1);
		Serial2.write(ch);
		if(ch == 'P') // start or stop Printing
			Serial2.write((char)27);

		if(lastChWasEsc || arduinoCommand){
			if(arduinoCommand == false && ch == 'c'){
				arduinoCommand = true;
			}else if(arduinoCommand){
				// TODO: weitern Befehlsinterpreter implementieren.
				if(ch == 'T'){ // Test-Befehl
					testCmd();
					arduinoCommand = false;
				}else if(ch == 'l'){ // reprint line: erneute ausgabe der aktuellen Messwertzeile erzwingen.
					lastWeighingValue += 1000;
					arduinoCommand = false;
				}else if(ch == 'H'){ // Heizung einschalten (manueller Modus mit 33°C Überhitzungsschutz)
					switchHeating(true);
					heatingAutoMode = false;
					arduinoCommand = false;
				}else if(ch == 'h'){ // Heizung ausschalten (manueller Modus mit 27°C "Frostschutz")
					switchHeating(false);
					heatingAutoMode = false;
					arduinoCommand = false;
				}else if(ch == 'a'){ // Automatik-modus der Heizung (32°C)
					heatingAutoMode = true;
					arduinoCommand = false;
				}else if(ch == 's'){ // Speichern des Regler-Zustandes
					saveRegState = true;
					arduinoCommand = false;
				}else if(ch == 'd'){ // Laden des Reglerzustandes
					loadRegState = true;
					arduinoCommand = false;
				}else if(ch == 'A'){ // Masse A auflegen
					loadMassA = true;
					arduinoCommand = false;
				}else if(ch == 'B'){ // Masse B auflegen
					loadMassB = true;
					arduinoCommand = false;
				}else if(ch == 'K'){ // beide Massen abnehmen und/oder ABBA-Zyklus abbrechen
					loadMassA = false;
					loadMassB = false;
					cyclicABBA = false;
					arduinoCommand = false;
				}else if(ch == 'Z'){ // automatische ABBA-Zyklen im 10-Minuten-Takt
					MC210_startCalibration();
					cyclicABBA = true;
					arduinoCommand = false;
				}else if(ch == 'i'){ // toggle
					if(!interpreterMode){
						Serial.println("Interpreter Mode active (CSV)");
						interpreterMode = true;
					}else{
						Serial.println("Direct Mode active (MC210S)");
						interpreterMode = false;
					}
					arduinoCommand = false;
				}else if(ch == '\n'){ // der interpreter wird definitiv bei einem Zeilenumbruch beendet.
					arduinoCommand = false;
				}
			}
		}

		if(ch == 27){
			lastChWasEsc = true;
		}else{
			lastChWasEsc = false;
		}
	}

	// Daten von der Waage:
	// nach einem \n wird die aktuelle zeile analysiert
	// 		und dann eine vollständige CSV-Zeile an den PC gesendet.
	// 		XON und XOFF werden direkt an den PC weitergeleitet.
	// 		nur neue Werte werden an den PC geschickt --> starke kompression bei statischen Signalen
	// 		die Umgebungsmesswerte werden gemittelt.
	//
	if(Serial2.available()){
		static char line[N_RX];
		static int iLine=0;
		static double lastTime = 27;


		Serial2.readBytes(&ch, 1);
		// an den PC weiterleiten:

		if((ch >= 17 && ch <=20) || !interpreterMode){
			Serial.write(ch);
		}else{
			line[iLine] = ch&0x7F;
			iLine++;
		}

		if(interpreterMode && ch == '\n'){
			line[iLine-1] = 0; // String abschließen, exkl. \n.
			Serial2.write(XOFF);

			//debug("Zeile interpretieren...\r\n");
			// TODO: neue zeile wird erkannt, länge passt auch, aber inhalt ist scheinbar unbrauchbar!
			// braucht es shifts? wegen 7-bit/8-bit???
			debug("len = ");
			debug(iLine);
			debug("\r\n");
			for(int i=0; line[i]; i++){
			//	debug(String(line[i], HEX) + " ");
			}
			debug(String(line));
			debug("\r\n");


			// Zeile interpretieren:
			// Beispiel: "N     -  0.00024 g  \r\n"
			double value = 0; // TODO: ist das so richtig?
			if(strncmp(line, "N ", 2) == 0){ // Zeile startet mit "N " --> Wägewert
				debug("Waegewert = ");
				int sign = +1;
				if(line[6] == '-'){
					sign = -1;
				}
				char tmp[10]={0};
				strncpy(tmp, line+7, 3);
				int grams = atoi(tmp)*sign; // Bugfix! 2016-09-06
				strncpy(tmp, line+11, 5);
				int32_t tenMicroGrams = atol(tmp);
				value = (double)grams + (double)tenMicroGrams*1e-5;
				value *= sign; // Messwert ist hier fertig!

				char str5[100];
				sprintf(str5, "%d.%05ld\r\n", grams, tenMicroGrams);
				debug(str5);


				int thisTime = lastTime;
				
				 if(!cyclicABBA){
				 	thisTime = minute(); 
				 	// neue Zeile spätestens einmal pro minute ausgeben, 
				 	// aber nur wenn nich im ABBA-Zyklus-Modus. 
				 	// denn ansonsten werden die Messwerte etwas verfälscht
				 	// (lineare Interpolation vs. Zero-Hold)
				 }
				 
				 static int lastMassState = 100;
				 
				if(((value != lastWeighingValue) && nAvg > 0) || 
						lastTime != thisTime ||
						(cyclicABBA && massState != lastMassState)){
					// neuen Wert ausgeben:
					printDateTime();

					// Temperatur, Druck und Luftfeuchte in 0.001°C, Pa und ppm
					char str[100];
					sprintf(str, "%+5ld;%7ld;%7ld;", (int32_t)(Tavg*1000), (int32_t)(sum_p/nAvg), (int32_t)(sum_phi/nAvg*10000));
					Serial.print(str);
					sprintf(str, "%d.%05ld;", grams, tenMicroGrams);
					Serial.print(str);
					Serial.print(String(P,3)+";"); // Heizleistung
					Serial.print(String(massState)+";");
					Serial.print("\r\n");

					nAvg = 1;
					sum_T = Tavg;
					sum_p = p;
					sum_phi = phi;

					lastWeighingValue = value;
					lastTime = thisTime;
					lastMassState = massState;
				} // end: neuer wert
				calibrating = false;
			} // wäegewert
			else if(strncmp(line, "Stat ", 5) == 0){ // Zeile startet mit "Stat " --> kalibrieren
				calibrating = true;
			}else{
				calibrating = false;
			}


			iLine = 0;
			Serial2.write(XON);
		} // end '\n'
	} // end Serial2.available()

	// Temperatur-Regler:
	temperatureController();
	weighingController();
	syncTime();
}

void testCmd()
{
	Serial.println("Test Command received");
}

// Format: 2016-08-15 21:34:59.987
void printDateTime()
{
	char str[40];
	int millis1;
	int millis2;

	do{
		millis1 = millis()%1000;
		sprintf(str, "%d-%02d-%02d %02d:%02d:%02d.%03d;", year(), month(), day(), hour(), minute(), second(), millis2=(int)(millis()%1000));
	}while(millis2 < millis1);
	// gab es während der string-erstellung einen überlauf, wird das ganze nochmals gemacht:

	Serial.write(str);
}

// Wartet, bis auf der Serial2 eine vorgegebene Zeile (templ) gefunden wurde.
// return false on timeOut
bool serial2WaitForLine(char* templ, int32_t timeOut_ms)
{
	char* line;
	bool found=false;
	int32_t millis1 = millis();
	int len = strlen(templ);

	do{
		line = serial2ReadLine(millis1-millis() + timeOut_ms);
		if(line && 0==strncmp(templ, line, len)){
			found = true;
		}
	}while(line!=0 && !found);

	if(line == 0){
		return false;
	}else
		return true;
}

// Zeile von der Waage einlesen mit einem timeout
// return: Pointer auf statischen Speicher
char* serial2ReadLine(int32_t timeOut_ms)
{
	const int N = 100;
	static char line[N];

	int32_t millis1 = millis();
	char ch;
	int i=0;
	bool timedOut=false;

	do{
		while(Serial2.available() == false && !timedOut){
			timedOut = (int32_t)millis() > (millis1+timeOut_ms);
		}

		Serial2.readBytes(&ch, 1);
		Serial.write(ch); // an den PC weiterleiten.
		if(ch != '\r')
			line[i] = ch;
		i++;

		// weiterleitung an die Waage:
		if(Serial.available()){
			char ch0;
			Serial.readBytes(&ch0, 1);
			Serial2.write(ch0);
		}
	}while(!timedOut && ch!='\n' && i<N);
	line[i-1] = 0;

	if(timedOut){
		debug("readLine timed out\r\n");
		return 0;
	}else{
		//debug("readLine="); debug(line); debug("\r\n");
		return line;
	}
}

float getTemperature()
{
	return bme280.readTemperature(); // return °C
}

float getPressure()
{
	return bme280.readPressure(); // return in Pa
}

float getHumidity()
{
	return bme280.readHumidity(); // return precent
}

void testBme280()
{
	while(1)
	{
		double T = getTemperature();
		double p = getPressure();
		double phi = getHumidity();
		char str[100];
		sprintf(str, "%+4d % 6ld % 3d \r\n", (int)(T*100), (int32_t)p, (int)(phi*10));
		Serial.print(str);
		delay(1000);
	}
}


// active high on pin 52
// 16W heating on 24V
void switchHeating(bool onFlag)
{
	digitalWrite(PIN_HEATING, onFlag);
}

// heating mode:
// manual, auto
void temperatureController()
{
	static double T_ref = 32;
	static double T_max = 34; // Überhitzungsschutz
	static double T_min = 27; // °C "Frostschutz"

	static const bool on = true;
	static const bool off = false;

	static const int32_t Period_ms = 10000; // milli sekunden, PWM-Zyklus
	static int32_t millisLastOn=0;

	// Überhitzungsschutz
	if(T > T_max){
		switchHeating(off);
		return;
	}
	// Frostschutz:
	if(T < T_min)
	{
		switchHeating(on);
		millisLastOn = millis();
		return;
	}

	if(heatingAutoMode || loadRegState || saveRegState)
	{
		static const double tau0 = 50e3; // Zeitkonstante in Sekunden des Gefrierschranks
		static const double tau = tau0/10; // Regler soll 10x schneller sein, als die natürliche Zeitkonstante.
		static const double kP = 20; // W/°C
		static const double kI = kP/tau; // W/K.s
		static const double kD = 3.0/(0.010/(5*60)); // 3W/ 10mK/5Minuten
		static const double P0 = 16; // Watt
		static bool firstRun = true;

		static const int Nb = 60; // -> 10 Minuten History

		struct RegState{
			double TavgBuffer[Nb];
			double T_errorIntegral; // Fehler-Integral in °C.s
		};

		static struct RegState rs;

		if(firstRun){
			firstRun = false;
			for(int i=0; i<Nb; i++){
				rs.TavgBuffer[i] = -1000; // mit ungültigen Werten füllen
			}
			rs.T_errorIntegral = 0;
		}

		if(loadRegState){
			loadRegState = false; // flag löschen
			EEPROM_readAnything(0, rs);
		}
		if(saveRegState){
			saveRegState = false; // flag löschen
			EEPROM_writeAnything(0, rs);
		}

		static int32_t millisOld = 0;

		if(heatingAutoMode && ((int32_t)millis() >=  (millisOld + Period_ms))){
			for(int i=0; i<(Nb-1); i++){
				rs.TavgBuffer[Nb-1-i] = rs.TavgBuffer[Nb-2-i];
			}
			rs.TavgBuffer[0] = Tavg;

			millisOld = millis();
			static double T_error = 0;
			T_error = T_ref-Tavg;
			static double T_errorDiff = 0;


			// Fehler-Differential (gemittelt über Nb/2-1 Werte)
			if(rs.TavgBuffer[Nb-1] > -300){ // warten, bis der Puffer voll ist
				T_errorDiff = (T_error - (T_ref - rs.TavgBuffer[Nb/2-1]))/((double)Period_ms*1.0e-3f*(Nb/2-1));
			}

			rs.T_errorIntegral += T_error*Period_ms/1000.0; // Fehlerintegral in K.s

			P = kI*rs.T_errorIntegral + kP*T_error + kD*T_errorDiff;

			// Anti-Windup:
			if(P<-P0/5 || P>P0/5){
				rs.T_errorIntegral -= T_error*Period_ms/1000.0; // Undo
			}

			P = constrain(P, 0, P0);


			switchHeating(on);
			millisLastOn = millis();
		}

		// PWM-Erzeugung:
		if((int32_t)millis() > (int32_t)(millisLastOn + Period_ms*P/P0)){
			switchHeating(off);
		}
	}
}


void weighingController()
{
	static bool firstRun = true;

	static const int pinA = 50;
	static const int pinB = 48;
	static const int pinEnable = 46;

	if(firstRun){
		firstRun = false;
		pinMode(pinA, OUTPUT);
		pinMode(pinB, OUTPUT);
		pinMode(pinEnable, OUTPUT);

		digitalWrite(pinA, HIGH);
		digitalWrite(pinB, HIGH);
		digitalWrite(pinEnable, LOW);
	}


	static uint32_t millisOld = 0;
	static const uint32_t Period_ms=60000; // one minute

	if(cyclicABBA){
		switch(massState){
		case State_A1:
			loadMassA=true;
			loadMassB=false;
			break;
		case State_B1:
			loadMassA=false;
			loadMassB=true;
			break;
		case State_B2:
			loadMassA=false;
			loadMassB=true;
			break;
		case State_A2:
			loadMassA=true;
			loadMassB=false;
			break;
		default:
			loadMassA = false;
			loadMassB = false;
		}

		if(millis() >= (millisOld + Period_ms) && !calibrating){
			millisOld = millis();
			massState++;
			massState = massState%State_End;

			// Start calibration before every ABBA cycle
			if(massState == State_01){
				MC210_startCalibration();
			}
		}
	}else{
		massState = State_01;
		millisOld = millis();
	}



	if(loadMassA){
		digitalWrite(pinA, LOW);
	}else{
		digitalWrite(pinA, HIGH);
	}

	if(loadMassB){
		digitalWrite(pinB, LOW);
	}else{
		digitalWrite(pinB, HIGH);
	}
}

// Diese Funktion braucht bis zu 1000ms!!
// selbständiges Synchronisieren einmal pro Stunde, wenn massState == 0
void syncTime()
{
	static uint32_t millisLastSync = 0;

	if(((millis() > (millisLastSync+3600000L)) && massState==0)
			|| millisLastSync == 0){
		DateTime now = rtc.now(); //get the current date-time
		while(millis()%1000); // warten auf eine runde milli-sekunden-zahl (Null), und dann uhrzeit einstellen:
		setTime(now.hour(), now.minute(), now.second(), now.date(), now.month(), now.year());
		millisLastSync = millis();
	}
}


void testRTC()
{
	uint32_t old_ts;
	while(1){
		DateTime now = rtc.now(); //get the current date-time
		uint32_t ts = now.getEpoch();

		if (old_ts == 0 || old_ts != ts) {
			old_ts = ts;
			Serial.print(now.year(), DEC);
			Serial.print('-');
			Serial.print(now.month(), DEC);
			Serial.print('-');
			Serial.print(now.date(), DEC);
			Serial.print(' ');
			Serial.print(now.hour(), DEC);
			Serial.print(':');
			Serial.print(now.minute(), DEC);
			Serial.print(':');
			Serial.print(now.second(), DEC);
			Serial.println();
		}
		delay(1000);
	}
}

void MC210_setFilter()
{
	// auf very stable umstellen
	char ch = 'K'; // very stable
	char str[20];
	sprintf(str, "%c%c\r\n", 27, ch);
	Serial2.print(str);
}

void MC210_startCalibration()
{
	char ch = 'Z'; // internal calibratiion
	char str[20];
	sprintf(str, "%c%c\r\n", 27, ch);
	Serial2.print(str);
}
