/*
 * servo.cpp
 *
 *  Created on: Aug 28, 2016
 *      Author: karl
 */

#include "servo.h"
#include <Arduino.h>
#include "EEPROMAnything.h"

int Servo::IdCounter=0;

Servo::Servo()
{
	id = IdCounter;
	IdCounter++;
}

void Servo::begin(uint8_t pin, int16_t unten, int16_t oben)
{
	this->pin = pin;
	unten_loops = unten;
	oben_loops = oben;

	pinMode(pin, OUTPUT);
	digitalWrite(pin,LOW);

	// langsames Herantasten an die Mitte
	winkel_loops = oben;
	sollWinkel_loops = winkel_loops;
	nextInitDelta = unten-oben;
	initialized = false;

	load();
}


void Servo::puls()
{
	if(!initialized){
		winkel_loops = winkel_loops + nextInitDelta;

		nextInitDelta *= (-1);
		if(nextInitDelta > 0)
			nextInitDelta--;
		else if(nextInitDelta < 0)
			nextInitDelta++;
		else{
			initialized = true;
			sollWinkel_loops = winkel_loops;
			pos_permill = 0.5;
		}
	}else{
		if(winkel_loops < sollWinkel_loops)
			winkel_loops++;
		else if(winkel_loops > sollWinkel_loops)
			winkel_loops--;
	}

	noInterrupts();
	digitalWrite(pin, HIGH);

	for(volatile int16_t i=0; i<winkel_loops; i++);

	digitalWrite(pin, LOW);
	interrupts();
}

void Servo::setPos(float w)
{
	sollWinkel_loops = (oben_loops-unten_loops)*w + unten_loops;
	pos_permill = w;
}

void Servo::save()
{
	magic1 = MAGIC1;
	magic2 = MAGIC2;
	EEPROM_updateAnything(sizeof(Serial)*id, *this);
	Serial.print("Saved Servo-ID ");
	Serial.println(id);
}

void Servo::load()
{
	Servo tmp;
	EEPROM_readAnything(sizeof(Serial)*id, tmp);

	if(tmp.magic1 == MAGIC1 &&
			tmp.magic2 == MAGIC2){


//		tmp.sollWinkel_loops = winkel_loops;
//		tmp.nextInitDelta = unten_loops-oben_loops;
//		tmp.initialized = false;

		*this = tmp;
		Serial.print("Loaded Servo-ID ");
		Serial.println(id);
	}else{
		Serial.print("Cannot load Servo-ID ");
		Serial.println(id);
	}
}

void Servo::setOben(int16_t oben)
{
	oben_loops = oben;
	setPos(pos_permill);
}
void Servo::setUnten(int16_t unten)
{
	unten_loops = unten;
	setPos(pos_permill);
}
