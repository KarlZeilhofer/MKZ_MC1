/*
 * servo.h
 *
 *  Created on: Aug 28, 2016
 *      Author: karl
 */

#ifndef SERVO_H_
#define SERVO_H_

#include <stdint.h>

#define MAGIC1 0x12345678
#define MAGIC2 0xabdf4321

class Servo
{
private:
	uint32_t magic1;

public:
	Servo();

	// bei 1000 loops per milli second:
	// 1000 ist normalerweise rechter Anschlag,
	// 2000 normalerweise der linke Anschlag.
	// --> mathematisch positiver Drehsinn
	void begin(uint8_t pin, int16_t unten_us, int16_t oben_us);

	void puls();

	void save();
	void load();

	/**
	 * unten = 0.0
	 * oben = 1.0
	 */
	void setPos(float p);

	// Winkel-Grenzen in loops:
private:
	int16_t oben_loops;
	int16_t unten_loops;
	int16_t winkel_loops; // aktueller Winkel
	int16_t sollWinkel_loops;

	uint8_t pin;

	// gemessen mit Arduino Nano, 16MHz:

	bool initialized;
	int16_t nextInitDelta;

	static int IdCounter;
	int id;
	float pos_permill;

public:
	static const int16_t LoopsPerMillis16=909;

	void setOben(int16_t oben);
	void setUnten(int16_t unten);
	int16_t oben(){return oben_loops;};
	int16_t unten(){return unten_loops;};
	float pos(){return pos_permill;};

private:
	uint32_t magic2;
};

#endif /* SERVO_H_ */
