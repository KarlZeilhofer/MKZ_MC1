

#include <Arduino.h>
#include "servo.h"
#include <string.h>


//int16_t loopsPerMillis16=909;
//int32_t loopsPerMillis32=454;

/*
 * Dieser Servo Controller steuert die Servos in der Masse-Komparator-Anwendung mit der
 * Sartorius MC210
 *
 * 		Dafür wird ein eigenes Board verwendet, um die Echtzeitfähigkeit, die notwendig für die Servos ist,
 * 		garantieren zu können.
 * 		Interrupts während der Pulserzeugung führen zu Jitter, und dies führt zu unruhigen Servos.
 *
 * 		Aufbau:
 *
 *
 * 		Servo1				Servo 2
 * 			\				 /
 * 			 \				/
 * 			  	  Masse 1
 *
 *
 *
 *
 *
 * 		Servo 3				Servo 4
 * 			\				 /
 * 			 \				/
 * 			  	  Masse 2
 *
 *
 *
 *
 */

// TOOD: every 4150s (+-100s) we miss one A1 and one B1 cycle.
// The bug could also be in the main controller firmware.

// Pins für Arduino Nano, Servo 1-5

Servo servos[4];
#define S1 A1
#define S2 A2
#define S3 A3
#define S4 A4
#define S5 A5

// Steuereingänge, acvitve LOW!
#define pinM1 2 // oben
#define pinM2 3 // unten
#define pinEnable 4 // enable


int state = 0;
bool nachLinks = true;

void setup() {
	Serial.begin(1200);

	servos[0].begin(A1, 2*Servo::LoopsPerMillis16, 1*Servo::LoopsPerMillis16);
	servos[1].begin(A2, 1*Servo::LoopsPerMillis16, 2*Servo::LoopsPerMillis16);
	servos[2].begin(A3, 2*Servo::LoopsPerMillis16, 1*Servo::LoopsPerMillis16);
	servos[3].begin(A4, 1*Servo::LoopsPerMillis16, 2*Servo::LoopsPerMillis16);




	Serial.println("Servo Controller");
	Serial.println("  usage: [a,b,c,d][h,u,l][value]");
	Serial.println("  select servo with a...d");
	Serial.println("  select hight (permill), upper or lower limit (in loop ticks)");
	Serial.println("  value: loop ticks for limits, permill for hight");
	Serial.println("  empty line for save parameters");


	pinMode(pinM1, INPUT);
	pinMode(pinM2, INPUT);
	pinMode(pinEnable, INPUT);

	// set pullup
	digitalWrite(pinM1, HIGH);
	digitalWrite(pinM2, HIGH);
	digitalWrite(pinEnable, HIGH);
}

void loopCLI()
{
	static char line[100]={0};
	static int i=0;

	if(Serial.available()){
		char ch;
		Serial.readBytes(&ch, 1);

		if(ch=='\r'){ // skip CR
			return;
		}
		line[i] = ch;
		i++;

		if(ch == '\n'){

			static int servo = 0;

			line[i-1]=0; // terminate string
			if(i==1){ // empty line = save
				servos[servo].save();
				i=0;
				return;
			}
			i=0;

			servo = line[0]-'a';
			int value = atoi(line+2);

			switch(line[1]){
			case 'h':{
				if(line[2] == 0)
					Serial.println((int16_t)(servos[servo].pos()*1000));
				else
					servos[servo].setPos((float)value/1000.0);
			}break;
			case 'u':{
				if(line[2] == 0)
					Serial.println(servos[servo].oben());
				else
					servos[servo].setOben(value);
			}break;
			case 'l':{
				if(line[2] == 0)
					Serial.println(servos[servo].unten());
				else
					servos[servo].setUnten(value);
			}break;
			}
		}
	}
}

void loop() {
	for(int i=0; i<4; i++){
		servos[i].puls();
		loopCLI();
	}

	static uint32_t millisStart = 0;

	// Pause for servos (about 16ms)
	millisStart = millis();
	while(millis() < (millisStart+16)){
		loopCLI();
	}


	if(digitalRead(pinEnable) == LOW){
		if(digitalRead(pinM1) == LOW){
			servos[0].setPos(0);
			servos[1].setPos(0);
		}else{
			servos[0].setPos(1.0);
			servos[1].setPos(1.0);
		}

		if(digitalRead(pinM2) == LOW){
			servos[2].setPos(0);
			servos[3].setPos(0);
		}else{
			servos[2].setPos(1.0);
			servos[3].setPos(1.0);
		}
	}
}
