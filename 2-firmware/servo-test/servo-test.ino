
int16_t loopsPerMillis16=909;
int32_t loopsPerMillis32=454;

/*
 * Dieses Test-Programm 
 *    fährt nach links, nach 2s
 *    fährt in die Mitte, nach 2s 
 *    fährt nach rechts, nach 2s
 *    einmal langsam von rechts nach links und zurück
 *    started wieder oben. 
 * 
 */


#define S1 A1
#define S2 A2
#define S3 A3
#define S4 A4
#define S5 A5

int state = 0;
bool nachLinks = true;

void setup() {
  // put your setup code here, to run once:

  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(S4, OUTPUT);
  pinMode(S5, OUTPUT);

  noInterrupts(); // no millis() !!!
}

void switchState()
{
  static int32_t called = 0;
  static int32_t calledOld = 0;

  if((state >=0 && state <= 2) && (called >= (calledOld + 100))){
    calledOld = called;
    state++;
  }else{
    if(state == 3 && nachLinks){
      calledOld = called;
      state = 0;
    }
  }
  

  called++;
}

void loop() {
  // put your main code here, to run repeatedly:

  // links = 1.0 ms
  // mitte = 1.5 ms
  // rechts = 2.0 ms
  static const int spd = 3; // speed, winkelschritt

  // Grenzen für Digital-Servo MC-1000MGBB (Waage)
  static const int winkelR = -100; // kleiner wert
  static const int winkelL = 800; // großer wert


  static int winkel=winkelR; 



  int16_t s = 0;
  int16_t e = (int16_t)(loopsPerMillis16 + (int32_t)winkel*loopsPerMillis16/1000);
  digitalWrite(S1, HIGH);
  digitalWrite(S2, HIGH);
  digitalWrite(S3, HIGH);
  digitalWrite(S4, HIGH);
  digitalWrite(S5, HIGH);
  for(volatile int16_t n=0; n<e; n++);
  digitalWrite(S1, LOW);
  digitalWrite(S2, LOW);
  digitalWrite(S3, LOW);
  digitalWrite(S4, LOW);
  digitalWrite(S5, LOW);

  // 18ms Pause
  for(volatile int16_t n=0; n<18*loopsPerMillis16; n++);


  switch(state){
    case 0: // links
      winkel = winkelL;
      switchState(); // nach 2 sekunden
      break;
    case 1: // mitte
      winkel = (winkelL+winkelR)/2;
      switchState(); // nach 2 sekunden
      break;
    case 2: // rechts
      winkel = winkelR;
      switchState(); // nach 2 sekunden
      break;
    case 3: // continuierlich
      if(nachLinks){
        winkel+=spd;
        if(winkel >= winkelL)
          nachLinks = false;
      }
      else{
        winkel-=spd;
        if(winkel <= winkelR){
          nachLinks = true;
          switchState();
        }
      }
      break;
  }  

  
}
