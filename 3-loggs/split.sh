#!/bin/bash

# shell script for plitting a long logfile into a file for each day. 

# parameter 1 = filename to be splitted (including .csv)
full=$1
base="$(basename $full .csv)"

mkdir $base

# find all dates
grep -Po "[0-9]{4}-[0-9]{2}-[0-9]{2}" "$base.csv" > all98754567_dates.txt

# find unique dates
sort -u all98754567_dates.txt > "$base"/dates.txt

# for each date d in dates.txt
cat "$base"/dates.txt | while read line
do
   baseo="$base"/"$line".csv
	grep -e "$line" "$base.csv" > "$baseo"
done

#rm dates.txt
rm all98754567_dates.txt

