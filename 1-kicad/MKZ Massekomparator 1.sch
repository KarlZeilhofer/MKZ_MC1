EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:zeilhofer
LIBS:controllino
LIBS:MKZ Massekomparator 1-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "MKZ Mass Comparator MC1"
Date "2016-08-31"
Rev "3.1 preview"
Comp "Mechatronik Karl Zeilhofer"
Comment1 "www.zeilhofer.co.at"
Comment2 "open source science"
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3500 1600 3100 4700
U 57C69F79
F0 "Klimaschrank" 60
F1 "klimaschrank.sch" 60
F2 "USB" B L 3500 3400 60 
F3 "P24V" I R 6600 3300 60 
F4 "M24V" I R 6600 3400 60 
F5 "P5V" I R 6600 4800 60 
F6 "M5V" I R 6600 4700 60 
F7 "P15V" I R 6600 2050 60 
F8 "M15V" I R 6600 2150 60 
$EndSheet
Text Notes 1000 3100 0    60   ~ 0
Netbook Lenovo 9e
Wire Notes Line
	900  3150 2400 3150
Wire Notes Line
	2400 3150 2400 4550
Wire Notes Line
	2400 4550 900  4550
Wire Notes Line
	900  4550 900  3150
Text Notes 950  4000 0    50   ~ 0
Xubuntu 12.04 32-bit\neth0: 10.0.0.200\nminicom (/dev/ttyACM0)\nopenssh-server (port 22)\napache2\nphp5\nr-base\n\nwebinterface (port 80)\nhttp://me.zeilhofer.co.at:20080/mc1
Text Notes 8500 3100 0    60   ~ 0
MeanWell 24V/25W
$Comp
L CONN_01X02 P?
U 1 1 57C6FCD0
P 8400 3350
F 0 "P?" H 8477 3388 50  0000 L CNN
F 1 "CONN_01X02" H 8477 3296 50  0000 L CNN
F 2 "" H 8400 3350 50  0000 C CNN
F 3 "" H 8400 3350 50  0000 C CNN
	1    8400 3350
	1    0    0    -1  
$EndComp
Wire Notes Line
	8400 3150 8400 3250
Wire Notes Line
	8400 3150 9400 3150
Wire Notes Line
	9400 3150 9400 3700
Wire Notes Line
	9400 3700 8400 3700
Wire Notes Line
	8400 3700 8400 3450
Wire Bus Line
	3500 3400 2400 3400
Wire Wire Line
	6600 3300 8200 3300
Wire Wire Line
	8200 3400 6600 3400
Text Notes 8400 5250 0    60   ~ 0
Dedicated power supply for the\nservo controller. 
$Comp
L CONN_01X05 P?
U 1 1 57C78C21
P 8400 4700
F 0 "P?" H 8477 4738 50  0001 L CNN
F 1 "CONN_01X02" H 8477 4646 50  0001 L CNN
F 2 "" H 8400 4700 50  0000 C CNN
F 3 "" H 8400 4700 50  0000 C CNN
	1    8400 4700
	1    0    0    -1  
$EndComp
Wire Notes Line
	8400 4350 8400 4450
Wire Notes Line
	8400 4350 9400 4350
Wire Notes Line
	9400 4350 9400 5050
Wire Notes Line
	9400 5050 8400 5050
Text Notes 8450 4300 0    60   ~ 0
MKZ PM5/11
Wire Notes Line
	8400 5050 8400 4950
Text Notes 8500 4900 0    60   ~ 0
-11\n-5\n0\n+5\n+11
Wire Wire Line
	6600 4700 8200 4700
Wire Wire Line
	8200 4800 6600 4800
Text Notes 4900 1000 0    100  ~ 20
MKZ Mass Comparator MC1
Text Notes 8350 1850 0    60   ~ 0
for Sartorius, 15V/15VA
$Comp
L CONN_01X02 P?
U 1 1 57C80736
P 8400 2100
F 0 "P?" H 8477 2138 50  0000 L CNN
F 1 "CONN_01X02" H 8477 2046 50  0000 L CNN
F 2 "" H 8400 2100 50  0000 C CNN
F 3 "" H 8400 2100 50  0000 C CNN
	1    8400 2100
	1    0    0    -1  
$EndComp
Wire Notes Line
	8400 1900 8400 2000
Wire Notes Line
	8400 1900 9400 1900
Wire Notes Line
	9400 1900 9400 2450
Wire Notes Line
	9400 2450 8400 2450
Wire Notes Line
	8400 2450 8400 2200
Wire Wire Line
	8200 2050 6600 2050
Wire Wire Line
	8200 2150 6600 2150
Wire Notes Line
	3250 1300 3250 6600
Wire Notes Line
	3250 6600 6900 6600
Wire Notes Line
	6900 6600 6900 1300
Wire Notes Line
	6900 1300 3250 1300
Text Notes 6200 6500 0    60   ~ 0
R_th = 2K/W
$EndSCHEMATC
