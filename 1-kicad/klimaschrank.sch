EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:zeilhofer
LIBS:controllino
LIBS:MKZ Massekomparator 1-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "MKZ Mass Comparator MC1"
Date "2016-09-07"
Rev "3.1 preview"
Comp "Mechatronik Karl Zeilhofer"
Comment1 "www.zeilhofer.co.at"
Comment2 "open source science"
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4100 3400 2000 1950
U 57C69FA1
F0 "Main Controller" 60
F1 "main-controller.sch" 60
F2 "USB" B L 4100 3600 60 
F3 "D52" O R 6100 4750 60 
F4 "D50" O R 6100 4850 60 
F5 "D48" O R 6100 4950 60 
F6 "D46" O R 6100 5050 60 
F7 "GND54" U R 6100 4650 60 
$EndSheet
$Sheet
S 4100 5950 2000 1050
U 57C69FC6
F0 "Servo Controller" 60
F1 "servo-controller.sch" 60
F2 "P5V" I R 6100 6800 60 
F3 "M5V" I R 6100 6900 60 
F4 "GND" I R 6100 6500 60 
F5 "MassA" I R 6100 6400 60 
F6 "MassB" I R 6100 6300 60 
F7 "Enable" I R 6100 6200 60 
$EndSheet
Text HLabel 1050 3600 0    60   BiDi ~ 0
USB
Text HLabel 10700 3650 2    60   Input ~ 0
P24V
Text HLabel 10700 3950 2    60   Input ~ 0
M24V
Text Notes 8050 3500 0    60   ~ 0
Heating 16W with NPN
$Comp
L Q_NPN_BCE Q?
U 1 1 57C6FE3C
P 8650 4600
F 0 "Q?" H 8844 4600 50  0000 L CNN
F 1 "Q_NPN_BCE" H 8844 4554 50  0001 L CNN
F 2 "" H 8850 4700 50  0000 C CNN
F 3 "" H 8650 4600 50  0000 C CNN
	1    8650 4600
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 57C6FF27
P 8750 4100
F 0 "R?" H 8820 4146 50  0000 L CNN
F 1 "16W" H 8820 4054 50  0000 L CNN
F 2 "" V 8680 4100 50  0000 C CNN
F 3 "" H 8750 4100 50  0000 C CNN
	1    8750 4100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P?
U 1 1 57C7000C
P 9400 3800
F 0 "P?" H 9477 3838 50  0001 L CNN
F 1 "CONN_01X02" H 9477 3792 50  0001 L CNN
F 2 "" H 9400 3800 50  0000 C CNN
F 3 "" H 9400 3800 50  0000 C CNN
	1    9400 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 3750 8750 3750
Wire Wire Line
	8750 3750 8750 3950
Wire Wire Line
	8750 4250 8750 4400
Wire Wire Line
	8750 4800 8750 5050
Wire Wire Line
	7950 5050 9200 5050
Wire Wire Line
	9200 5050 9200 3850
Connection ~ 8750 5050
$Comp
L R R?
U 1 1 57C7011D
P 8200 4600
F 0 "R?" V 7992 4600 50  0000 C CNN
F 1 "1k" V 8084 4600 50  0000 C CNN
F 2 "" V 8130 4600 50  0000 C CNN
F 3 "" H 8200 4600 50  0000 C CNN
	1    8200 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	8450 4600 8350 4600
$Comp
L CONN_01X02 P?
U 1 1 57C701A3
P 7750 4800
F 0 "P?" H 7828 4792 50  0001 L CNN
F 1 "CONN_01X02" H 7828 4746 50  0001 L CNN
F 2 "" H 7750 4800 50  0000 C CNN
F 3 "" H 7750 4800 50  0000 C CNN
	1    7750 4800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7950 4850 7950 5050
Wire Wire Line
	7950 4750 7950 4600
Wire Wire Line
	7950 4600 8050 4600
Text Notes 8650 4250 1    60   ~ 0
Cu wire
Wire Notes Line
	9400 3700 9400 3550
Wire Notes Line
	9400 3550 7750 3550
Wire Notes Line
	7750 3550 7750 4700
Wire Notes Line
	7750 4900 7750 5200
Wire Notes Line
	7750 5200 9400 5200
Wire Notes Line
	9400 5200 9400 3900
Wire Wire Line
	9450 3850 10550 3850
Wire Wire Line
	10550 3850 10550 3950
Wire Wire Line
	10550 3950 10700 3950
Wire Wire Line
	10700 3650 10550 3650
Wire Wire Line
	10550 3650 10550 3750
Wire Bus Line
	1050 3600 4100 3600
Text Notes 2200 4950 0    60   ~ 0
Mass A
Text Notes 2200 5750 0    60   ~ 0
Mass B
Text Notes 1450 4850 1    60   ~ 0
Servo 0
Text Notes 3550 4850 1    60   ~ 0
Servo 1
Text Notes 1450 5700 1    60   ~ 0
Servo 2
Text Notes 3550 5700 1    60   ~ 0
Servo 3
Wire Notes Line
	1800 4450 1800 6200
Wire Notes Line
	1800 6200 3000 6200
Wire Notes Line
	3000 6200 3000 4450
Wire Notes Line
	3000 4450 1800 4450
Wire Notes Line
	1800 5300 3000 5300
Wire Notes Line
	2350 4450 2350 4350
Wire Notes Line
	2350 4350 2450 4350
Wire Notes Line
	2450 4350 2450 4450
Text Notes 1800 6300 0    60   ~ 0
Copper Frame
Wire Notes Line
	2400 4350 2400 2950
Wire Notes Line
	1300 3850 3600 3850
Wire Notes Line
	3600 3850 3600 7150
Wire Notes Line
	3600 7150 1300 7150
Wire Notes Line
	1300 7150 1300 3850
Wire Notes Line
	1300 4400 1500 4400
Wire Notes Line
	1500 4400 1500 4950
Wire Notes Line
	1500 4950 1300 4950
Wire Notes Line
	1300 5250 1500 5250
Wire Notes Line
	1500 5250 1500 5850
Wire Notes Line
	1500 5850 1300 5850
Wire Notes Line
	3600 5250 3400 5250
Wire Notes Line
	3400 5250 3400 5850
Wire Notes Line
	3400 5850 3600 5850
Wire Notes Line
	3600 4950 3400 4950
Wire Notes Line
	3400 4950 3400 4400
Wire Notes Line
	3400 4400 3600 4400
Wire Notes Line
	1200 3750 3700 3750
Wire Notes Line
	3700 3750 3700 7250
Wire Notes Line
	3700 7250 1200 7250
Wire Notes Line
	1200 7250 1200 3750
Text Notes 2500 7400 0    60   ~ 0
Wooden Weighing Chamber
Text HLabel 10700 6150 2    60   Input ~ 0
P5V
Text HLabel 10700 6300 2    60   Input ~ 0
M5V
Wire Wire Line
	6100 6800 6800 6800
Wire Wire Line
	6800 6800 6800 6150
Wire Wire Line
	6800 6150 10700 6150
Wire Wire Line
	6100 6900 6850 6900
Wire Wire Line
	6850 6900 6850 6300
Wire Wire Line
	6850 6300 10700 6300
Wire Wire Line
	6100 4750 7700 4750
Wire Wire Line
	6100 4850 6500 4850
Wire Wire Line
	6500 4850 6500 6400
Wire Wire Line
	6500 6400 6100 6400
Wire Wire Line
	6100 4950 6450 4950
Wire Wire Line
	6450 4950 6450 6300
Wire Wire Line
	6450 6300 6100 6300
Wire Wire Line
	6100 5050 6400 5050
Wire Wire Line
	6400 5050 6400 6200
Wire Wire Line
	6400 6200 6100 6200
Wire Wire Line
	6100 4650 6550 4650
Wire Wire Line
	6550 4650 6550 6500
Wire Wire Line
	6550 6500 6100 6500
Wire Wire Line
	7700 4850 6550 4850
Connection ~ 6550 4850
Wire Notes Line
	5800 6500 5550 6500
Wire Notes Line
	5550 6500 5550 6900
Wire Notes Line
	5550 6900 5800 6900
Text HLabel 10700 1750 2    60   Input ~ 0
P15V
Text HLabel 10700 1900 2    60   Input ~ 0
M15V
Wire Wire Line
	5050 1900 10700 1900
Wire Wire Line
	5050 1750 10700 1750
Text Notes 1650 1050 0    100  ~ 20
Sartorius MC210S
Wire Notes Line
	1650 1150 3000 1150
Wire Notes Line
	3000 1150 3000 2850
Wire Notes Line
	800  2850 5050 2850
Wire Notes Line
	1650 2850 1650 1150
Wire Notes Line
	1500 2550 800  2700
Wire Notes Line
	800  2700 800  2850
Wire Notes Line
	1500 2550 1500 2800
Wire Notes Line
	1500 2800 1650 2800
Wire Notes Line
	3000 2650 3100 2650
Wire Notes Line
	3100 2650 3100 1450
Wire Notes Line
	3100 1450 5050 1200
Wire Notes Line
	5050 1200 5050 2850
Wire Notes Line
	1800 2850 1800 2950
Wire Notes Line
	1800 2950 1900 2950
Wire Notes Line
	1900 2950 1900 2850
Wire Notes Line
	4400 2850 4400 2950
Wire Notes Line
	4400 2950 4500 2950
Wire Notes Line
	4500 2950 4500 2850
Text Notes 4550 1800 0    60   ~ 0
center pin
Text Notes 4750 1950 0    60   ~ 0
outer
Wire Bus Line
	1400 4400 1400 3950
Wire Bus Line
	1400 3950 3300 3950
Wire Bus Line
	3300 3950 3300 5100
Wire Bus Line
	3300 5100 3900 5100
Wire Bus Line
	3900 5100 3900 6350
Wire Bus Line
	3900 6350 4100 6350
Wire Bus Line
	1400 5250 1400 5100
Wire Bus Line
	1400 5100 1550 5100
Wire Bus Line
	1550 5100 1550 4300
Wire Bus Line
	1550 4300 1400 4300
Wire Bus Line
	3500 4400 3500 4300
Wire Bus Line
	3500 4300 3300 4300
Wire Bus Line
	3500 5250 3500 5100
Wire Bus Line
	6100 3700 6550 3700
Wire Bus Line
	6550 3700 6550 2200
Wire Bus Line
	6550 2200 5050 2200
Text Notes 6500 3250 1    60   ~ 0
RS232 (RxD,TxD)
$Comp
L RELAY_1CH_NC K?
U 1 1 57D03F39
P 10000 3850
F 0 "K?" H 9993 3878 60  0001 L CNN
F 1 "Thermo-Switch 40°C" V 9700 3850 60  0000 C CNN
F 2 "" H 10000 3850 60  0000 C CNN
F 3 "" H 10000 3850 60  0000 C CNN
	1    10000 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	9450 3750 9800 3750
Wire Wire Line
	10550 3750 10150 3750
$EndSCHEMATC
