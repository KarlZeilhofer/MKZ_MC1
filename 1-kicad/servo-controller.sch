EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:zeilhofer
LIBS:controllino
LIBS:MKZ Massekomparator 1-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "MKZ Mass Comparator MC1"
Date "2016-08-31"
Rev "3.1 preview"
Comp "Mechatronik Karl Zeilhofer"
Comment1 "www.zeilhofer.co.at"
Comment2 "open source science"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Arduino-Nano-R3 U?
U 1 1 57C6A236
P 5500 1750
F 0 "U?" H 5450 2737 60  0000 C CNN
F 1 "Arduino-Nano-R3" H 5450 2631 60  0000 C CNN
F 2 "" H 5000 1600 60  0000 C CNN
F 3 "" H 5000 1600 60  0000 C CNN
	1    5500 1750
	1    0    0    -1  
$EndComp
Text Label 4050 1450 0    60   ~ 0
massA\
Text Label 4050 1550 0    60   ~ 0
massB\
Text Label 4050 1650 0    60   ~ 0
enable\
NoConn ~ 4750 1050
NoConn ~ 4750 1150
Text Label 4050 1350 0    60   ~ 0
GND
NoConn ~ 4750 1250
NoConn ~ 4750 1750
NoConn ~ 4750 1850
NoConn ~ 4750 1950
NoConn ~ 4750 2050
NoConn ~ 4750 2150
NoConn ~ 4750 2250
NoConn ~ 4750 2350
NoConn ~ 4750 2450
$Comp
L CONN_01X10 P?
U 1 1 57C6A378
P 6700 1600
F 0 "P?" H 6650 2200 50  0000 L CNN
F 1 "CONN_01X08" H 6350 950 50  0001 L CNN
F 2 "" H 6700 1600 50  0000 C CNN
F 3 "" H 6700 1600 50  0000 C CNN
	1    6700 1600
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P?
U 1 1 57C6A47C
P 8600 1750
F 0 "P?" V 8472 1928 50  0000 L CNN
F 1 "Servo0" V 8564 1928 50  0000 L CNN
F 2 "" H 8600 1750 50  0000 C CNN
F 3 "" H 8600 1750 50  0000 C CNN
	1    8600 1750
	0    1    1    0   
$EndComp
$Comp
L CONN_01X03 P?
U 1 1 57C6A52C
P 8600 2250
F 0 "P?" V 8472 2428 50  0000 L CNN
F 1 "Servo1" V 8564 2428 50  0000 L CNN
F 2 "" H 8600 2250 50  0000 C CNN
F 3 "" H 8600 2250 50  0000 C CNN
	1    8600 2250
	0    1    1    0   
$EndComp
$Comp
L CONN_01X03 P?
U 1 1 57C6A558
P 8600 2750
F 0 "P?" V 8472 2928 50  0000 L CNN
F 1 "Servo2" V 8564 2928 50  0000 L CNN
F 2 "" H 8600 2750 50  0000 C CNN
F 3 "" H 8600 2750 50  0000 C CNN
	1    8600 2750
	0    1    1    0   
$EndComp
$Comp
L CONN_01X03 P?
U 1 1 57C6A5A1
P 8600 3300
F 0 "P?" V 8472 3478 50  0000 L CNN
F 1 "Servo3" V 8564 3478 50  0000 L CNN
F 2 "" H 8600 3300 50  0000 C CNN
F 3 "" H 8600 3300 50  0000 C CNN
	1    8600 3300
	0    1    1    0   
$EndComp
$Comp
L CONN_01X03 P?
U 1 1 57C6A5DB
P 8600 3850
F 0 "P?" V 8472 4028 50  0000 L CNN
F 1 "Servo4" V 8564 4028 50  0000 L CNN
F 2 "" H 8600 3850 50  0000 C CNN
F 3 "" H 8600 3850 50  0000 C CNN
	1    8600 3850
	0    1    1    0   
$EndComp
$Comp
L CONN_01X03 P?
U 1 1 57C6A616
P 8600 4350
F 0 "P?" V 8472 4528 50  0000 L CNN
F 1 "Servo5" V 8564 4528 50  0000 L CNN
F 2 "" H 8600 4350 50  0000 C CNN
F 3 "" H 8600 4350 50  0000 C CNN
	1    8600 4350
	0    1    1    0   
$EndComp
$Comp
L CONN_01X10 P?
U 1 1 57C6A99E
P 7550 1600
F 0 "P?" H 7600 2200 50  0000 L CNN
F 1 "CONN_01X08" H 7600 950 50  0001 L CNN
F 2 "" H 7550 1600 50  0000 C CNN
F 3 "" H 7550 1600 50  0000 C CNN
	1    7550 1600
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X03 P?
U 1 1 57C6B1D2
P 8600 4900
F 0 "P?" V 8472 5078 50  0000 L CNN
F 1 "Servo6" V 8564 5078 50  0000 L CNN
F 2 "" H 8600 4900 50  0000 C CNN
F 3 "" H 8600 4900 50  0000 C CNN
	1    8600 4900
	0    1    1    0   
$EndComp
$Comp
L CONN_01X03 P?
U 1 1 57C6B1D8
P 8600 5400
F 0 "P?" V 8472 5578 50  0000 L CNN
F 1 "Servo7" V 8564 5578 50  0000 L CNN
F 2 "" H 8600 5400 50  0000 C CNN
F 3 "" H 8600 5400 50  0000 C CNN
	1    8600 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	1100 1450 4750 1450
Wire Wire Line
	1100 1550 4750 1550
Wire Wire Line
	1100 1650 4750 1650
Wire Wire Line
	1100 1350 4750 1350
Wire Wire Line
	6150 1150 6500 1150
Wire Wire Line
	6150 1350 6500 1350
Wire Wire Line
	6150 1650 6500 1650
Wire Wire Line
	6150 1750 6500 1750
Wire Wire Line
	6150 1850 6500 1850
Wire Wire Line
	6150 1950 6500 1950
Wire Wire Line
	6150 2050 6500 2050
Wire Wire Line
	8250 1350 8250 5150
Wire Wire Line
	7750 1350 8600 1350
Wire Wire Line
	8600 1350 8600 1550
Wire Wire Line
	8600 2050 8600 2000
Wire Wire Line
	8600 2000 8250 2000
Connection ~ 8250 2000
Wire Wire Line
	8250 4100 8600 4100
Wire Wire Line
	8600 4100 8600 4150
Wire Wire Line
	8600 3650 8600 3600
Wire Wire Line
	8600 3600 8250 3600
Connection ~ 8250 3600
Wire Wire Line
	8600 3100 8600 3050
Wire Wire Line
	8600 3050 8250 3050
Connection ~ 8250 3050
Wire Wire Line
	8600 2550 8600 2500
Wire Wire Line
	8600 2500 8250 2500
Connection ~ 8250 2500
Wire Wire Line
	8250 5150 8600 5150
Wire Wire Line
	8600 5150 8600 5200
Wire Wire Line
	8600 4700 8600 4650
Wire Wire Line
	8600 4650 8250 4650
Connection ~ 8250 4650
Connection ~ 8250 4100
Wire Wire Line
	8700 1550 8700 1300
Wire Wire Line
	8700 1300 8350 1300
Wire Wire Line
	8350 1150 8350 5100
Wire Wire Line
	8350 5100 8700 5100
Wire Wire Line
	8700 5100 8700 5200
Wire Wire Line
	8350 4600 8700 4600
Wire Wire Line
	8700 4600 8700 4700
Connection ~ 8350 4600
Wire Wire Line
	8350 4050 8700 4050
Wire Wire Line
	8700 4050 8700 4150
Connection ~ 8350 4050
Wire Wire Line
	8350 3550 8700 3550
Wire Wire Line
	8700 3550 8700 3650
Connection ~ 8350 3550
Wire Wire Line
	8350 3000 8700 3000
Wire Wire Line
	8700 3000 8700 3100
Connection ~ 8350 3000
Wire Wire Line
	8350 2450 8700 2450
Wire Wire Line
	8700 2450 8700 2550
Connection ~ 8350 2450
Wire Wire Line
	8350 1950 8700 1950
Wire Wire Line
	8700 1950 8700 2050
Connection ~ 8350 1950
Connection ~ 8250 1350
Wire Wire Line
	7750 1150 8350 1150
Connection ~ 8350 1300
Wire Wire Line
	7750 2050 7850 2050
Wire Wire Line
	7850 2050 7850 1550
Wire Wire Line
	7850 1550 8500 1550
Wire Wire Line
	7750 1950 7900 1950
Wire Wire Line
	7900 1950 7900 2050
Wire Wire Line
	7900 2050 8500 2050
Wire Wire Line
	7750 1850 7950 1850
Wire Wire Line
	7950 1850 7950 2550
Wire Wire Line
	7950 2550 8500 2550
Wire Wire Line
	7750 1750 8000 1750
Wire Wire Line
	8000 1750 8000 3100
Wire Wire Line
	8000 3100 8500 3100
Wire Wire Line
	7750 1650 8050 1650
Wire Wire Line
	8050 1650 8050 3650
Wire Wire Line
	8050 3650 8500 3650
Wire Wire Line
	8500 4150 8050 4150
Wire Wire Line
	8500 4700 8050 4700
Wire Wire Line
	8500 5200 8050 5200
NoConn ~ 8050 5200
NoConn ~ 8050 4700
NoConn ~ 8050 4150
Wire Notes Line
	7550 1100 7550 900 
Wire Notes Line
	7550 900  10400 900 
Wire Notes Line
	10400 6050 7550 6050
Wire Notes Line
	7550 6050 7550 2100
Text Notes 7600 850  0    60   ~ 0
Servo Distribution Board
Entry Wire Line
	7750 1350 7850 1250
Entry Wire Line
	7750 1150 7850 1050
Wire Wire Line
	7850 1250 10200 1250
Wire Wire Line
	7850 1050 10200 1050
$Comp
L CONN_01X02 P?
U 1 1 57C6D779
P 10400 1150
F 0 "P?" H 10478 1142 50  0001 L CNN
F 1 "CONN_01X02" H 10477 1096 50  0001 L CNN
F 2 "" H 10400 1150 50  0000 C CNN
F 3 "" H 10400 1150 50  0000 C CNN
	1    10400 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 1050 10200 1100
Wire Wire Line
	10200 1250 10200 1200
Wire Notes Line
	10400 900  10400 1050
Wire Notes Line
	10400 1250 10400 6050
Text HLabel 10800 1100 2    60   Input ~ 0
P5V
Text HLabel 10800 1200 2    60   Input ~ 0
M5V
Wire Wire Line
	10800 1200 10450 1200
Wire Wire Line
	10450 1100 10800 1100
Text HLabel 1100 1350 0    60   Input ~ 0
GND
Text HLabel 1100 1450 0    60   Input ~ 0
MassA
Text HLabel 1100 1550 0    60   Input ~ 0
MassB
Text HLabel 1100 1650 0    60   Input ~ 0
Enable
Text Notes 4750 2850 0    60   ~ 0
Firmware: MC210-ServoController
Wire Notes Line
	2600 1400 3350 1400
Wire Notes Line
	3350 1400 3350 1700
Wire Notes Line
	3350 1700 2600 1700
Wire Notes Line
	2600 1700 2600 1400
Text Notes 800  3200 0    60   ~ 0
Digital Controll Inputs. \nThe external servo controll signals are accepted\nby the servo controller, when the enable\ signal is low. \n\nWhen massA\ is low, weight A is put onto the copper frame of the scale. \nWhen massB\ is low, weight B is put onto the copper frame of the scale. \n\nThe servos need some seconds to move the masses gently. \n\nPullups in Arduino Nano enabled (to +5V)!
Wire Notes Line
	2600 1700 2050 2250
Wire Notes Line
	2050 2250 750  2250
$EndSCHEMATC
