EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:zeilhofer
LIBS:controllino
LIBS:MKZ Massekomparator 1-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "MKZ Mass Comparator MC1"
Date "2016-08-31"
Rev "3.1 preview"
Comp "Mechatronik Karl Zeilhofer"
Comment1 "www.zeilhofer.co.at"
Comment2 "open source science"
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 10200 5950 0    60   ~ 0
to Servo Controller
Text Notes 8850 1550 0    60   ~ 0
Arduino Mega 2560
Text Notes 5750 1800 0    60   ~ 0
RS232-Driver
$Comp
L CONN_01X04 A?
U 1 1 57C6A091
P 2450 2000
F 0 "A?" V 3650 2500 50  0000 R CNN
F 1 "BME280" V 2400 1750 50  0000 R CNN
F 2 "" H 2450 2000 50  0000 C CNN
F 3 "" H 2450 2000 50  0000 C CNN
	1    2450 2000
	-1   0    0    1   
$EndComp
Wire Notes Line
	2450 2200 2450 2500
Wire Notes Line
	2450 2500 1300 2500
Wire Notes Line
	1300 2500 1300 1450
Wire Notes Line
	1300 1450 2450 1450
Wire Notes Line
	2450 1450 2450 1800
Text HLabel 10700 4550 2    60   BiDi ~ 0
USB
Text HLabel 9600 6100 2    60   Output ~ 0
D52
Text HLabel 9600 6000 2    60   Output ~ 0
D50
Text HLabel 9600 5900 2    60   Output ~ 0
D48
Text HLabel 9600 5800 2    60   Output ~ 0
D46
Text HLabel 9600 6200 2    60   UnSpc ~ 0
GND54
Wire Notes Line
	9900 5800 10050 5800
Wire Notes Line
	10050 5800 10050 6000
Wire Notes Line
	10050 6000 9900 6000
Wire Notes Line
	9900 5900 10150 5900
Text Notes 10200 6100 0    60   ~ 0
to heating
$Comp
L CONN_01X04 A?
U 1 1 57C88B15
P 2450 3300
F 0 "A?" V 3650 3800 50  0000 R CNN
F 1 "DS3231" V 2400 3050 50  0000 R CNN
F 2 "" H 2450 3300 50  0000 C CNN
F 3 "" H 2450 3300 50  0000 C CNN
	1    2450 3300
	-1   0    0    1   
$EndComp
Wire Notes Line
	2450 3500 2450 3800
Wire Notes Line
	2450 3800 1300 3800
Wire Notes Line
	1300 3800 1300 2750
Wire Notes Line
	1300 2750 2450 2750
Wire Notes Line
	2450 2750 2450 3100
Wire Notes Line
	8300 1650 8300 5450
Wire Notes Line
	8300 5450 10300 5450
Wire Notes Line
	10300 5450 10300 1650
Wire Notes Line
	10300 1650 8300 1650
Wire Notes Line
	1550 1650 1550 1900
Wire Notes Line
	1550 1900 1800 1900
Wire Notes Line
	1800 1900 1800 1650
Wire Notes Line
	1800 1650 1550 1650
Wire Notes Line
	1700 1750 1700 1800
Wire Notes Line
	1700 1800 1750 1800
Wire Notes Line
	1750 1800 1750 1750
Wire Notes Line
	1750 1750 1700 1750
Text Notes 2200 2200 0    60   ~ 0
SDA\nSCL\nGND\nVIN
Text Notes 1800 2450 0    60   ~ 0
top view
Wire Notes Line
	1400 2200 1400 2300
Wire Notes Line
	1400 2300 1450 2350
Wire Notes Line
	1450 2350 1550 2350
Wire Notes Line
	1550 2350 1600 2300
Wire Notes Line
	1600 2300 1600 2200
Wire Notes Line
	1600 2200 1550 2150
Wire Notes Line
	1550 2150 1450 2150
Wire Notes Line
	1450 2150 1400 2200
$EndSCHEMATC
