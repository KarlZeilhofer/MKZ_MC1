#include <stdio.h>
#include <string.h>
#include <stdbool.h>

// logfile example:
/*
2016-07-23T23:22:52.439 2918 N     -  0.00024
2016-07-23T23:22:52.837 2918 N     -  0.00024
2016-07-23T23:22:53.237 2918 N     -  0.00024
2016-07-23T23:22:53.637 2918 N     -  0.00024 g
2016-07-23T23:22:54.036 2918 N     -  0.00024 g
2016-07-23T23:22:54.436 2918 N     -  0.00024 g
2016-07-23T23:22:54.836 2918 N     -  0.00024 g
2016-07-23T23:22:55.237 2918 N     -  0.00024 g

the last 14 characters represent the weighing value.
*/

#define LEN 200

int main(int argc, char *argv[])
{
	if(argc == 1)
	{
		printf("Usage:\n");
		printf("comp <logfile>\n");

	}

	if(argc == 2)
	{
		FILE* fptr = fopen(argv[1], "rt");
		char line[LEN] = {0};
		char lastLine[LEN] = {0};
		int lastLen=0;

		if(!fptr){
			printf("cannot open file %s\n", argv[1]);
			return -1;
		}

		fgets(line, LEN-1, fptr); // skip first line, which is possibly not complete!
		fgets(line, LEN-1, fptr);
//		printf("%s", line);
		strcpy(lastLine, line);
		lastLen = strlen(line);

		while(!feof(fptr))
		{
			fgets(line, LEN-1, fptr);
			int len = strlen(line);

			static const int N = 14; // compare last 14 characters

			if(len>=N && lastLen >= N && strncmp(line+len-N, lastLine+lastLen-N, N)!=0 &&
					strncmp(line+len-21, "N ", 2) == 0){

				// prepare this line:
				// remove unnecessary spaces, remove this "N", replace g/space with 1/0:

				char line2[LEN+1];
				strncpy(line2, line, len-21);
				int i = len-21;
				int len2 = len-21;
				i++; // skip N
				while(line[i] == ' ' && i<len) // skip spaces after N
					i++;
				line2[len2] = line[i]; // +/-
				i++; len2++;
				while(line[i] == ' ' && i<len) // skip spaces after sign
					i++;

				while(line[i] != ' ' && i<len){ // copy number
					line2[len2] = line[i];
					i++;
					len2++;
				}
				line2[len2] = line[i]; // copy space
				i++; len2++;
				line2[len2] = line[i]=='g'?'1':'0';  // replace g/space with 1/0:
				i++;len2++;
				line2[len2]=0;
				len2++;

				char line3[LEN+1];
				// remove multiple spaces and replace them with a semicolon:
				int i2=0; int i3=0;
				bool lastCharWasSpace = false;
				while(line2[i2]){
					if(line2[i2] == ' '){
						if(!lastCharWasSpace){
							lastCharWasSpace = true;
							line3[i3++] = ';';
						}
					}else{
						lastCharWasSpace = false;
						if(line2[i2] == 'T') // replace the T of the time-stamp
							line3[i3++] = ' ';
						else
							line3[i3++] = line2[i2];

					}
					i2++;
				}
				line3[i3++] = ';';
				line3[i3++] = 0;

				printf("%s\n", line3);
				strcpy(lastLine, line);
				lastLen = len;
			}
		}

	}
	return 0;
}
