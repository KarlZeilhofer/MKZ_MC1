#!/bin/bash

# plot 72 hours of data of current.csv
# see also config file Rconfig_hours

cd /home/karl/MC1/4-Rcode
R --no-save < plotcurrentcsv.R > output.txt
