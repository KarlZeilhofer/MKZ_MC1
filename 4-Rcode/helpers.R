

# Funktion zum extrahieren des Anzeigewertes
# id: Zustand, aus dem der Wert extrahiert werden soll
# t: Zeitvektor, beginnend bei 0 in Sekunden
# w: Anzeigewert (Gewicht) in g
# s: Zustandsvektor über der Zeit (0-8)
getValue = function(id, t, w, s)
{
  value = 0;
  
  # range where state s is id
  isID = (id == s);
  r1 = min(which(isID))
  r2 = max(which(isID))

  r = r1:r2
  
  wS = w[r];
  tS = t[r]-t[r[1]];
  
  
  # gültige werte sind im Zeit-Intervall 10:50 Sekunden.
  ti = seq(15,25,by=0.2)
  wi = dointerpolation(tS, wS, ti)
  uWi = sqrt(var(wi))
  
  
  if(uWi > 10e-6 && PRINT_DIST)
  {  
    plot(ti,(wi-mean(wi))*1e6, type='s', xlim=c(0,60), lwd=2,col="blue", main = "Excessive standard deviation (>10ug)", 
         xlab="Zeit in s", ylab="Abweichung vom Mittelwert in ug");
    lines(tS,(wS-mean(wi))*1e6, xlim=c(0,60))
    legend("right", sprintf("sigma = %3.1f ug", uWi*1e6))
    value = NA;
  }else{
    value = mean(wi);
  }
  
  return(value);
}




dointerpolation = function(tRaw, xRaw, ti)
{
  xi = 0 # interpolated data, to be calculated
  itRaw = 1 # index for tRaw
  for(iti in 1:length(ti)) # for every interpolation time-stamp
  {
    if(length(tRaw) == 1 && ti[iti] >= tRaw[itRaw]) # special case: we have only a single raw point in a complete interval
    {
      xi[iti] = xRaw[itRaw];
    }else
    {
      if(ti[iti] < tRaw[itRaw])
      {
        xi[iti] = NA;
      }else # we have a valid x1
      {
        # now we increment x1 as long it is > xi, and then we decrement it once again
        while(tRaw[itRaw] <= ti[iti])
        {
          if(itRaw<length(tRaw))
          {
            itRaw = itRaw+1;
          }else
          {
            break
          }
        }
        itRaw = itRaw-1; 
        # now we have the nearest neighbour left or equal to xi
        
        i1 = itRaw;
        i2 = ifelse((itRaw+1)<=length(tRaw),itRaw+1,length(tRaw))
        x1 = tRaw[i1];
        x2 = tRaw[i2];
        y1 = xRaw[i1];
        y2 = xRaw[i2];
        
        xi[iti] = interpolate(x1,x2,ti[iti],y1,y2);
      }
    }
  }
  
  return(xi)
}

# linear interpolation
# if xi < x1 or y1 or y2 are NA, 
# it returns NA
# if xi is out of [x1:x2], do a step interpolation
interpolate = function(x1,x2,xi,y1,y2)
{
  if(xi < x1)
    return(NA)
  if(xi > x2)
    return(y2)
  if(is.na(y1) || is.na(y2))
    return(NA)
  if(x1 == x2)
    return(y1)
  yi = (y2-y1)/(x2-x1)*(xi-x1)+y1
  return(yi)
}


printf = function(s, ...) cat(paste0(sprintf(s, ...)), '\n') # define printf with linefeed
printf_nlf = function(s, ...) cat(paste0(sprintf(s, ...))) # define printf with no linefeed

