Massekomparator MC1
===================


# Bedienung der Waage über Minicom
## Verbindungsparameter
* Software Flusssteuerung (Xon, Xoff)
* 9600 8N1

## Ablauf
* Minicom innerhalb von screen starten, siehe http://www.zeilhofer.co.at/wiki/doku.php?id=remote_serial_terminal
* ESC + ci: startet den logging modus, anstatt dem direkten interaktiven modus
* ESC + cZ: startet messzyklen (ABBA)
* vor verlassen des Messplatzes sollte screen detachet werden (Ctrl+A D)

## Remote Bedienung
* starten von screen mit Parameter -r (resume)


# Installation am Netbook
* Webserver der eine Einfach PHP Seite anbietet, womit PDF angesehen werden können
* crontab Eingrag, der je um 5, 11, 17 und 23 Uhr das Skript /MC1/4-Rcode/plotdata.sh ausführt
* Dieses erstellt PDF-Datein im entsprechenden Unterordner in 5-plots über die vergangenen 6h der Messdaten. 

## Starten einer neuen Messung
* minicom: logdatei schließen (CTRL+A L)
* Kopieren von 3-loggs/current.csv nach z.B. 306.csv
* Umschreiben der Datei /3-loggs/current.txt auf die neue messungsnummer
* minicom: logging starten (wieder CTRL+A L)

# Ungelöste Probleme
## Riesige Logdaten und Git
Die Messung 306 zB hat 900MB als csv-Datei. 

So eine Datei sollte nicht per git gespeichert werden. 
Bei der Messung ist vermutlich was schief gelaufen. Normalerweise werden Zeilen nur geschrieben, wenn es eine Änderung gibt. 

### Ursache
* Evt. `millis()` überlauf? 
* Oder Thermoschalter hat angesprochen? (heute, 19.8.2018, geht die Heizung wieder, ich glaub, die ging einmal nicht mehr). 

## Automatische Justage der Waage
Da die Waage eine CE0 Variante ist, kann man die automatische Justierung nicht ausschalten, soweit ich mich noch erinnern kann. Sollte geprüft werden. Dies führt unter Umständen zu systematischen Messfehlern. 

